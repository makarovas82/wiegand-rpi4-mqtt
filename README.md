# wiegand rpi4 mqtt

1. поставить mosquitto-dev
2. поставить wiringPi совместимый c gpio RPI4 (https://raspberrypi.ru/578-wiring-pi-biblioteka-raboty-s-gpio-chast-1)
3. скомпилить gcc reader.c -o reader.o -lmosquitto -lwiringPi
4. запускать с аргументами ./reader.o 0 1 localhost 1883 wiegand/gate1

0 - gpio D0 (WiringPi нумерация)\
1 - gpio D1 (WiringPi нумерация)\
localhost - ip mqtt брокера\
1883 - порт брокера\
wiegand/gate1 - топик куда постить код\


GPIO см тут: https://pinout.xyz/pinout/5v_power
